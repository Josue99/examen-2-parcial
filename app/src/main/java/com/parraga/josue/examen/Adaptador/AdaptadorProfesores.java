package com.parraga.josue.examen.Adaptador;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parraga.josue.examen.Modelo.Profesores;
import com.parraga.josue.examen.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdaptadorProfesores extends RecyclerView.Adapter<AdaptadorProfesores.MyViewHolder> {

    private ArrayList<Profesores> profesores;

    public AdaptadorProfesores(ArrayList<Profesores> profesores){ this.profesores = profesores; }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_profesor, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        Profesores profesores1 = profesores.get(position);
        myViewHolder.id.setText(profesores1.getId());
        myViewHolder.cedula.setText(profesores1.getCedula());
        myViewHolder.titular.setText(profesores1.getTitular());
        myViewHolder.nombre.setText(profesores1.getNombre());
        myViewHolder.apellido.setText(profesores1.getApellido());
        myViewHolder.profesion.setText(profesores1.getProfesion());
        myViewHolder.materia.setText(profesores1.getMateria());
        Picasso.get().load(profesores1.getImagen()).into(myViewHolder.imagen);
    }

    @Override
    public int getItemCount() { return profesores.size(); }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imagen;
        private TextView id, cedula, titular, nombre, apellido, profesion, materia;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imagen = (ImageView)itemView.findViewById(R.id.Imagen);
            id = (TextView)itemView.findViewById(R.id.Id);
            cedula = (TextView)itemView.findViewById(R.id.Cedula);
            titular = (TextView)itemView.findViewById(R.id.Titular);
            nombre = (TextView)itemView.findViewById(R.id.Nombre);
            apellido = (TextView)itemView.findViewById(R.id.Apellido);
            profesion = (TextView)itemView.findViewById(R.id.Profesion);
            materia = (TextView)itemView.findViewById(R.id.Materia);

        }
    }
}
