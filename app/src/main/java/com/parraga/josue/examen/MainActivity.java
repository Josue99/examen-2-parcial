package com.parraga.josue.examen;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.parraga.josue.examen.Adaptador.AdaptadorProfesores;
import com.parraga.josue.examen.Modelo.Profesores;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private static final String URL_PROFESORES = "http://10.22.14.179:3003/profesores/";
    private RecyclerView.LayoutManager layoutManager;
    private AdaptadorProfesores adaptadorProfesores;
    private ArrayList<Profesores> arrayListP;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView)findViewById(R.id.Profesores);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        arrayListP = new ArrayList<>();
        adaptadorProfesores = new AdaptadorProfesores(arrayListP);
        progressDialog = new ProgressDialog(this);

        ObtenerProfesores();
    }

    private void ObtenerProfesores() {
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PROFESORES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response);
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++){
                        JSONObject jsonObject =jsonArray.getJSONObject(i);
                        Profesores profesores = new Profesores();
                        profesores.setImagen(jsonObject.getString("imagen"));
                        profesores.setId(jsonObject.getString("id"));
                        profesores.setCedula(jsonObject.getString("cedula"));
                        profesores.setTitular(jsonObject.getString("titular"));
                        profesores.setNombre(jsonObject.getString("nombre"));
                        profesores.setApellido(jsonObject.getString("apellido"));
                        profesores.setProfesion(jsonObject.getString("profesion"));
                        profesores.setMateria(jsonObject.getString("materia"));
                        arrayListP.add(profesores);
                    }
                    recyclerView.setAdapter(adaptadorProfesores);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Ocurrio un error mientras se cargaba", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
